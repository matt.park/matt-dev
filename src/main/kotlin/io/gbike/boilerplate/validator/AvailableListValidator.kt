package io.gbike.boilerplate.validator

import io.gbike.boilerplate.annotation.ValidAvailableList
import jakarta.validation.ConstraintValidator
import jakarta.validation.ConstraintValidatorContext

class AvailableListValidator : ConstraintValidator<ValidAvailableList, String> {
    private lateinit var targetFieldName: String
    private lateinit var availableList: Array<String>

    override fun initialize(constraintAnnotation: ValidAvailableList) {
        targetFieldName = constraintAnnotation.targetFieldName
        availableList = constraintAnnotation.availableList
    }

    override fun isValid(value: String?, context: ConstraintValidatorContext?): Boolean {
        if (value == null) {
            return false
        }

        return value in availableList
    }
}