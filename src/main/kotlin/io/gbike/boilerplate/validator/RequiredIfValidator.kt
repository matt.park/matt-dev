package io.gbike.boilerplate.validator

import io.gbike.boilerplate.annotation.ValidRequiredIf
import org.springframework.beans.BeanWrapperImpl
import jakarta.validation.ConstraintValidator
import jakarta.validation.ConstraintValidatorContext

class RequiredIfValidator : ConstraintValidator<ValidRequiredIf, Any> {
    private lateinit var targetFieldName: String
    private lateinit var dependFieldName: String
    private lateinit var dependFieldValues: List<String>

    override fun initialize(constraintAnnotation: ValidRequiredIf) {
        targetFieldName = constraintAnnotation.targetFieldName
        dependFieldName = constraintAnnotation.dependFieldName
        dependFieldValues = constraintAnnotation.dependFieldValue.split("|").map { it.trim() }
    }

    override fun isValid(value: Any, context: ConstraintValidatorContext): Boolean {
        val expectedDependFieldValue = BeanWrapperImpl(value).getPropertyValue(this.dependFieldName)
        val targetFieldValue = BeanWrapperImpl(value).getPropertyValue(this.targetFieldName)

        return !(expectedDependFieldValue in dependFieldValues && targetFieldValue == null)
    }
}