package io.gbike.boilerplate.validator

import io.gbike.boilerplate.annotation.ValidAvailableEnum
import io.gbike.boilerplate.enums.ValidEnum
import jakarta.validation.ConstraintValidator
import jakarta.validation.ConstraintValidatorContext

class AvailableEnumValidator : ConstraintValidator<ValidAvailableEnum, String> {
    private lateinit var targetFieldName: String
    private lateinit var availableList: List<String>
    override fun initialize(constraintAnnotation: ValidAvailableEnum) {
        targetFieldName = constraintAnnotation.targetFieldName
        availableList = constraintAnnotation.enumClass.java.enumConstants.filter { it is ValidEnum }
            .map { if (it is ValidEnum) it.value else "" }
    }

    override fun isValid(value: String?, context: ConstraintValidatorContext?): Boolean {
        if (value == null) {
            return false
        }

        return value in availableList
    }
}