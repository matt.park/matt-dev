package io.gbike.boilerplate.annotation

import io.gbike.boilerplate.validator.AvailableListValidator
import jakarta.validation.Constraint
import jakarta.validation.Payload
import kotlin.reflect.KClass


@Constraint(validatedBy = [AvailableListValidator::class])
@Target(AnnotationTarget.FIELD, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
@Retention(AnnotationRetention.RUNTIME)
annotation class ValidAvailableList(
    val message: String = "{validation.ValidAvailableList}",
    val groups: Array<KClass<*>> = [],
    val payload: Array<KClass<out Payload>> = [],
    val targetFieldName: String = "",
    val availableList: Array<String> = []
)
