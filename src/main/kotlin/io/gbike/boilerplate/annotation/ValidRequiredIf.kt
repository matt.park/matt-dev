package io.gbike.boilerplate.annotation

import io.gbike.boilerplate.validator.RequiredIfValidator
import jakarta.validation.Constraint
import jakarta.validation.Payload
import kotlin.reflect.KClass


@Constraint(validatedBy = [RequiredIfValidator::class])
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Repeatable
annotation class ValidRequiredIf(
    val message: String = "{validation.ValidRequiredIf}",
    val groups: Array<KClass<*>> = [],
    val payload: Array<KClass<out Payload>> = [],
    val targetFieldName: String = "",
    val dependFieldName: String = "",
    val dependFieldValue: String = ""
)