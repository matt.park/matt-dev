package io.gbike.boilerplate.annotation

import io.gbike.boilerplate.validator.AvailableEnumValidator
import jakarta.validation.Constraint
import jakarta.validation.Payload
import kotlin.reflect.KClass

@Constraint(validatedBy = [AvailableEnumValidator::class])
@Target(
    AnnotationTarget.FIELD,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER,
    AnnotationTarget.VALUE_PARAMETER
)
@Retention(AnnotationRetention.RUNTIME)
annotation class ValidAvailableEnum(
    val message: String = "{validation.ValidAvailableEnum}",
    val groups: Array<KClass<*>> = [],
    val payload: Array<KClass<out Payload>> = [],
    val targetFieldName: String = "",
    val enumClass: KClass<out Enum<*>>
)