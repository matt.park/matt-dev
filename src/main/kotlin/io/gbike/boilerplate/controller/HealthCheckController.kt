package io.gbike.boilerplate.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class HealthCheckController {

    @GetMapping(path = ["/"])
    fun healthCheck(): String {
        return "gbike"
    }
}