package io.gbike.boilerplate.util

import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort

class PageRequest {
    private var page: Int = 1
    private var size: Int = 10
    private var direction: Sort.Direction = Sort.Direction.DESC

    // 페이지네이션 사용 여부 / false로 주면 모든 데이터를 가져옴
    private var paged = true


    fun setPage(page: Int) {
        this.page = if (page <= 0) 1 else page
    }

    fun setSize(size: Int) {
        val DEFAULT_SIZE = 10
        val MAX_SIZE = Int.MAX_VALUE
        this.size = if (size > MAX_SIZE) DEFAULT_SIZE else size
    }

    fun setDirection(direction: Sort.Direction) {
        this.direction = direction
    }

    fun setPaged(paged: Boolean) {
        this.paged = paged
    }

    fun of(): Pageable {
        return if(paged) {
            PageRequest.of(page - 1, size, direction, "id")
        } else {
            Pageable.unpaged()
        }
    }
}