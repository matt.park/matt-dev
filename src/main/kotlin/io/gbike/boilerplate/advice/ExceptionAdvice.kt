package io.gbike.boilerplate.advice

import io.gbike.boilerplate.dto.response.CommonResponse
import io.gbike.boilerplate.enums.ErrorCodeType
import io.gbike.boilerplate.exception.GbikeException
import io.sentry.Sentry
import jakarta.validation.ConstraintViolationException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.servlet.NoHandlerFoundException
import java.net.http.HttpConnectTimeoutException

@RestControllerAdvice
class ExceptionAdvice {
    @ExceptionHandler(value = [GbikeException::class])
    fun handleCommonException(e: GbikeException): ResponseEntity<CommonResponse.Error> {
        Sentry.captureException(e)
        val errorCode = e.errorCode!!

        return ResponseEntity.status(errorCode.httpStatus).body(CommonResponse.Error().apply {
            this.message = errorCode.message
            this.error = CommonResponse.ErrorDetail().apply {
                this.detailCode = errorCode.code
            }
        })
    }

    @ExceptionHandler(value = [NoHandlerFoundException::class])
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    fun handleNoHandlerFoundException(e: NoHandlerFoundException): ResponseEntity<CommonResponse.Error> {
        Sentry.captureException(e)
        val errorCode: ErrorCodeType = ErrorCodeType.NOT_FOUND

        return ResponseEntity.status(errorCode.httpStatus).body(CommonResponse.Error(errorCode.message).apply {
            this.error = CommonResponse.ErrorDetail().apply {
                this.detailCode = errorCode.code
            }
        })
    }

    @ExceptionHandler(value = [ConstraintViolationException::class])    // pathVariable validation 실패 시 발생시키는 에러
    fun handleConstraintViolationException(e: ConstraintViolationException): ResponseEntity<CommonResponse.Error> {
        Sentry.captureException(e)
        val errorCode: ErrorCodeType = ErrorCodeType.NOT_FOUND

        return ResponseEntity.status(errorCode.httpStatus).body(CommonResponse.Error().apply {
            this.message = errorCode.message
            this.error = CommonResponse.ErrorDetail().apply {
                this.detailCode = errorCode.code
                this.detailMessage = e.message.toString()
            }
        })
    }

    @ExceptionHandler(value = [MethodArgumentNotValidException::class]) // Validation 어노테이션이 작동하여 조건에 맞지 않았을 때 발생시키는 에러
    fun handleMethodArgumentNotValidException(e: MethodArgumentNotValidException): ResponseEntity<CommonResponse.Error> {
        Sentry.captureException(e)
        val errorCode: ErrorCodeType = ErrorCodeType.REQUIRED_FIELD

        val fieldErrorMessageList: List<String?> = e.allErrors.map { it.defaultMessage }

        return ResponseEntity.status(errorCode.httpStatus).body(CommonResponse.Error().apply {
            this.message = errorCode.message
            this.error = CommonResponse.ErrorDetail().apply {
                this.detailCode = errorCode.code
                this.detailMessage = fieldErrorMessageList.joinToString("|")
            }
        })
    }

    @ExceptionHandler(value = [HttpMessageNotReadableException::class]) // Request Body 형식(JSON) 이 맞지 않거나, 형태를 지키지 않았을 경우
    fun handleHttpMessageNotReadableException(e: HttpMessageNotReadableException): ResponseEntity<CommonResponse.Error> {
        Sentry.captureException(e)
        val errorCode: ErrorCodeType = ErrorCodeType.METHOD_NOT_FOUND

        return ResponseEntity.status(errorCode.httpStatus).body(CommonResponse.Error().apply {
            this.message = errorCode.message
            this.error = CommonResponse.ErrorDetail().apply {
                this.detailMessage = e.message.toString()
            }
        })
    }

    @ExceptionHandler(value = [HttpConnectTimeoutException::class]) // http client 커넥션 타입아웃
    fun handleHttpConnectionTimeOutException(e: HttpMessageNotReadableException): ResponseEntity<CommonResponse.Error> {
        Sentry.captureException(e)
        val errorCode: ErrorCodeType = ErrorCodeType.HTTP_CLIENT_TIMEOUT

        return ResponseEntity.status(errorCode.httpStatus).body(CommonResponse.Error().apply {
            this.message = errorCode.message
            this.error = CommonResponse.ErrorDetail().apply {
                this.detailMessage = e.message.toString()
            }
        })
    }

    @ExceptionHandler(value = [Exception::class]) // 그 외 모든 에러
    fun handleException(e: Exception): ResponseEntity<CommonResponse.Error> {
        Sentry.captureException(e)
        val errorCode: ErrorCodeType = ErrorCodeType.COMMON_ERROR

        return ResponseEntity.status(errorCode.httpStatus).body(CommonResponse.Error().apply {
            this.message = errorCode.message
            this.error = CommonResponse.ErrorDetail().apply {
                this.detailMessage = e.message.toString()
            }
        })
    }
}