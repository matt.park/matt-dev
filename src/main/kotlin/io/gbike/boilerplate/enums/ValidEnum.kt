package io.gbike.boilerplate.enums

/**
 * Interface ValidEnum
 * -------------------------------
 * ValidAvailableEnum 에 사용할 Enum 은 해당 인터페이스를 상속받아야한다.
 * (value attribute 가 무조건 존재한다는 가정이 필요해서..)
 */
interface ValidEnum {
    val value: String
}