package io.gbike.boilerplate.enums.configuration

enum class RDSConnectionType {
    WRITE, READ_ONLY
}