package io.gbike.boilerplate.enums

import org.springframework.http.HttpStatus

enum class ErrorCodeType(val code: String, val message: String, val httpStatus: HttpStatus = HttpStatus.BAD_REQUEST) {
    COMMON_ERROR("E-9999", "알 수 없는 오류가 발생했습니다.", HttpStatus.INTERNAL_SERVER_ERROR),

    NOT_FOUND("E-0001", "찾을 수 없는 API 입니다.", HttpStatus.NOT_FOUND),
    METHOD_NOT_FOUND("E-0002", "잘못된 요청입니다.", HttpStatus.METHOD_NOT_ALLOWED),
    REQUIRED_FIELD("E-0003", "필수 파라미터가 누락되었습니다."),

    RECORD_NOT_FOUND("E-0100", "데이터를 찾을 수 없습니다.", HttpStatus.BAD_REQUEST),

    HTTP_CLIENT_ERROR("C-0001", "API 통신 오류", HttpStatus.INTERNAL_SERVER_ERROR),
    HTTP_CLIENT_TIMEOUT("C-0002", "TimeOut", HttpStatus.REQUEST_TIMEOUT),
    NO_DATA_RETURN("C-0003", "body 데이터 오류", HttpStatus.INTERNAL_SERVER_ERROR)
}