package io.gbike.boilerplate.repository

import jakarta.persistence.EntityManager
import jakarta.persistence.PersistenceContext
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport
import org.springframework.stereotype.Repository

@Repository
abstract class QueryDslRepositorySupport(domainClass: Class<*>?) : QuerydslRepositorySupport(domainClass!!) {
    @PersistenceContext
    override fun setEntityManager(entityManager: EntityManager) {
        super.setEntityManager(entityManager)
    }

    /**
     * UPDATE / DELETE 후 실행
     */
    protected fun clear(): Unit {
        super.getEntityManager()?.clear()
    }
}