package io.gbike.boilerplate.exception

import io.gbike.boilerplate.enums.ErrorCodeType


class GbikeException : RuntimeException {
    var code: String = ""
    override var message: String = ""
    var errorCode: ErrorCodeType? = null

    constructor() : super(null, null)
    constructor(errorCode: ErrorCodeType) : super(errorCode.message, null) {
        this.errorCode = errorCode
        this.code = errorCode.code
        this.message = errorCode.message
    }
}