package io.gbike.boilerplate.configuration.datasource

import io.gbike.boilerplate.enums.configuration.RDSConnectionType
import org.slf4j.LoggerFactory
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource
import org.springframework.transaction.support.TransactionSynchronizationManager
import javax.sql.DataSource

class LazyReplicationConnectionDataSource(
    writeDataSource: DataSource, readOnlyDataSource: DataSource
) : AbstractRoutingDataSource() {
    init {
        super.setTargetDataSources(
            mapOf(
                RDSConnectionType.WRITE to writeDataSource,
                RDSConnectionType.READ_ONLY to readOnlyDataSource,
            )
        )
    }

    override fun determineCurrentLookupKey(): Any {
        val routingType =
            if (TransactionSynchronizationManager.isCurrentTransactionReadOnly()) RDSConnectionType.READ_ONLY
            else RDSConnectionType.WRITE
        LOGGER.debug("Datasource is routed to $routingType")
        return routingType
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(this::class.java)
    }
}
