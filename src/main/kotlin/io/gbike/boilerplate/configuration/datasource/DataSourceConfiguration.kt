package io.gbike.boilerplate.configuration.datasource

import com.zaxxer.hikari.HikariDataSource
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.jdbc.datasource.LazyConnectionDataSourceProxy
import org.springframework.orm.jpa.JpaTransactionManager
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean
import org.springframework.transaction.PlatformTransactionManager
import org.springframework.transaction.annotation.EnableTransactionManagement
import javax.sql.DataSource


@Configuration
@EnableAutoConfiguration(
    exclude = [
        DataSourceAutoConfiguration::class,
    ]
)
@EnableTransactionManagement
class DataSourceConfiguration {

    @Bean(name = ["writeDataSource"])
    @ConfigurationProperties(prefix = "spring.datasource.write")
    fun writeDataSource(): DataSource {
        return HikariDataSource()
    }

    @Bean(name = ["readDataSource"])
    @ConfigurationProperties(prefix = "spring.datasource.read")
    fun readOnlyDataSource(): DataSource {
        return HikariDataSource()
    }

    @Bean(name = ["routingDataSource"])
    fun routingDataSource(
        @Qualifier("writeDataSource") writeDataSource: DataSource,
        @Qualifier("readDataSource") readDataSource: DataSource,
    ): DataSource {
        // Custom Routing DataSource
        return LazyReplicationConnectionDataSource(writeDataSource, readDataSource)
    }

    @Primary
    @Bean(name = ["dataSource"])
    fun dataSource(
        @Qualifier("routingDataSource") routingDataSource: DataSource,
    ): LazyConnectionDataSourceProxy {
        return LazyConnectionDataSourceProxy(routingDataSource)
    }

    @Primary
    @Bean(name = ["entityManagerFactory"])
    fun primaryEntityManagerFactory(
        builder: EntityManagerFactoryBuilder, @Qualifier("dataSource") dataSource: DataSource?
    ): LocalContainerEntityManagerFactoryBean? {
        return builder.dataSource(dataSource).packages("io.gbike.boilerplate...domain")
            .persistenceUnit("entityManager")
            .build()
    }

    @Primary
    @Bean
    fun primaryTransactionManager(
        @Qualifier("entityManagerFactory") entityManagerFactory: LocalContainerEntityManagerFactoryBean?
    ): PlatformTransactionManager? {
        return JpaTransactionManager(entityManagerFactory!!.`object`!!)
    }
}

