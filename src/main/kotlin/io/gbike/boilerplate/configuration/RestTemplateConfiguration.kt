package io.gbike.boilerplate.configuration

import org.apache.hc.client5.http.impl.classic.HttpClientBuilder
import org.apache.hc.client5.http.impl.io.PoolingHttpClientConnectionManagerBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.client.ClientHttpResponse
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory
import org.springframework.stereotype.Component
import org.springframework.web.client.ResponseErrorHandler
import org.springframework.web.client.RestTemplate

@Configuration
class RestTemplateConfiguration {
    @Bean
    fun restTemplate(): RestTemplate {
        val factory = HttpComponentsClientHttpRequestFactory()

        factory.httpClient = HttpClientBuilder.create().setConnectionManager(
            PoolingHttpClientConnectionManagerBuilder.create().setMaxConnTotal(50).setMaxConnPerRoute(20).build()
        ).build()

        val restTemplate = RestTemplate(factory)
        restTemplate.errorHandler = RestTemplateResponseErrorHandler()

        return restTemplate
    }

    @Component
    class RestTemplateResponseErrorHandler : ResponseErrorHandler {
        override fun hasError(response: ClientHttpResponse): Boolean {
            return (response.statusCode.isError)
        }

        override fun handleError(response: ClientHttpResponse) {
            // 별도로 http 에러코드에 대한 에러처리를 하지 않는다.
        }
    }
}