package io.gbike.boilerplate.dto.response

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming

/**
 * 공통 응답 response 데이터
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
open class CommonResponse {
    open var code: Int? = null
    open var message: String = ""
    open var response: Any? = null
    open var error: ErrorDetail? = null

    @JsonNaming(value = PropertyNamingStrategies.SnakeCaseStrategy::class)
    @JsonPropertyOrder("code", "message", "response", "error")
    data class Success(
        override var response: Any? = null
    ) : CommonResponse() {
        override var code: Int? = 0
        override var message: String = "성공"
    }

    @JsonNaming(value = PropertyNamingStrategies.SnakeCaseStrategy::class)
    @JsonPropertyOrder("code", "message", "response", "error")
    data class Error(
        override var message: String = "실패",
    ) : CommonResponse() {
        override var code: Int? = 1
        override var error: ErrorDetail? = null
    }

    @JsonNaming(value = PropertyNamingStrategies.SnakeCaseStrategy::class)
    @JsonPropertyOrder("detail_code", "detail_message")
    data class ErrorDetail(
        var detailCode: String = "",
        var detailMessage: String = "",
    )
}
