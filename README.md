# GBike Kotlin SpringBoot Boilerplate

---

[Confluence Link](https://gbiketeam.atlassian.net/wiki/spaces/G1/pages/711065601/230327+Kotlin+SpringBoot)

- JDK 17
- Kotlin 1.7.22
- SpringBoot 3.0.4

### packages
- QueryDSL 5.0.0
- HttpClient5
